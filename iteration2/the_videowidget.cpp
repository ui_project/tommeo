//
// Created by sc18br on 11/12/19.
//

#include <iostream>
#include "the_videowidget.h"

using namespace std;


void TheVWidget::mouseDoubleClickEvent(QMouseEvent *e){
    if(e->button() == Qt::LeftButton) {
        if(!isFullscreen()) {
            //creates a new window to full screen into
            window->setCentralWidget(this);
            window->setWindowFlags(Qt::FramelessWindowHint | Qt::CustomizeWindowHint);
            window->showFullScreen();
            setFullscreen(true);
        } else {
            //hide new window and fire signal
            window->hide();
            setFullscreen(false);
            fullscreenChanged();
        }
    }
    QVideoWidget::mouseDoubleClickEvent(e);
}

bool TheVWidget::isFullscreen() {
    return fullscreen;
}

void TheVWidget::setFullscreen(bool fs) {
    fullscreen = fs;
}

void TheVWidget::resetPlayer(QMainWindow *window, TheVWidget *vWidget) {
    window->setCentralWidget(vWidget);
}
