//
// Created by twak on 11/11/2019.
//

#include "the_button.h"


void TheButton::init(TheButtonInfo* i) {
    setIcon( *(i->icon) );
    info =  i;
}


void TheButton::clicked() {
    delete this;
}

void TheButton::showX() {
    setIcon(*delete_icon);
}

void TheButton::showIcon() {
    setIcon(* info->icon);
}

void TheButton::enterEvent( QEvent* e )
{
    Q_EMIT hovered();

    // don't forget to forward the event
    QWidget::enterEvent( e );
}

void TheButton::leaveEvent( QEvent* e )
{
    Q_EMIT left();

    // don't forget to forward the event
    QWidget::enterEvent( e );
}