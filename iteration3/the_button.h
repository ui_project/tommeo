//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H


#include <QtWidgets/QPushButton>
#include <QTimer>

class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display

    TheButtonInfo ( QUrl* url, QIcon* icon) : url (url), icon (icon) {}
};

class TheButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;
    QTimer* mTimer;

     TheButton(QWidget *parent) :  QPushButton(parent) {
         setMouseTracking(true);
         setIconSize(QSize(200,110));
         connect(this, SIGNAL(clicked()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
         connect( this, SIGNAL(hovered()), this, SLOT(showX()) );
         connect( this, SIGNAL(left()), this, SLOT(showIcon()) );
     }

    void init(TheButtonInfo* i);

protected:
    virtual void enterEvent( QEvent* e );
    virtual void leaveEvent( QEvent* e);

private:
    QIcon *delete_icon = new QIcon("icons/delete.png");

private slots:
    void clicked();

public slots:
    void showX();
    void showIcon();

signals:
    void jumpTo(TheButtonInfo*);
    void hovered();
    void left();

};

#endif //CW2_THE_BUTTON_H
