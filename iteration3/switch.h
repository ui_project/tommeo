//
// Created by harlan on 12/12/19.
//

#ifndef CW2_SWITCH_H
#define CW2_SWITCH_H


#include <QtWidgets/QPushButton>

using namespace std;

class TheSwitch : public QPushButton {
    Q_OBJECT

public:
    TheSwitch( const QString& text, QWidget* parent = nullptr );

public slots:
    void swapIcons();

private slots:
    void addClick();

private:
    int counter = 0;
    QIcon *play_icon = new QIcon("icons/play1.png");
    QIcon *pause_icon = new QIcon("icons/pause1.png");
};

#endif //CW2_SWITCH_H
