//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i){
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}

// change the image and video for one button every one second
void ThePlayer::playNext() {
    video++;
    if(video > 6) {
        video = 0;
    }
    TheButtonInfo* i = & infos -> at (video); //updateCount++ % infos->size() );
    setMedia(*i->url);
    playPause();
}

void ThePlayer::playPrevious() {
    video--;
    if(video < 0) {
        video = 6;
    }
    TheButtonInfo* i = & infos -> at (video);
    setMedia(*i->url);
    playPause();
}

void ThePlayer::addClick() {
    counter++;
}

void ThePlayer::playPause() {
    if (counter % 2 == 0){
        play();
        mTimer->start();
        paused = false;
    }
    else {
        pause();
        mTimer->stop();
        paused = true;
    }
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            if(paused == false) {
                play(); // starting playing again...//
            }
    }
}


void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

