//
// Created by sc18br on 11/12/19.
//

#ifndef CW2_THE_VIDEOWIDGET_H
#define CW2_THE_VIDEOWIDGET_H

#include <QApplication>
#include <QVideoWidget>
#include <QMouseEvent>
#include <QMainWindow>

using namespace std;

class TheVWidget : public QVideoWidget {
Q_OBJECT

public:
    QMainWindow *window = new QMainWindow();
    bool isFullscreen();
    void setFullscreen(bool fs);

public slots:
    void resetPlayer(QMainWindow *window, TheVWidget *vWidget);

signals:
    void fullscreenChanged();

private:
    void mouseDoubleClickEvent(QMouseEvent *e);
    bool fullscreen;

};

#endif //CW2_THE_VIDEOWIDGET_H
