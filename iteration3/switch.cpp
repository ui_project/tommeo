//
// Created by harlan on 12/12/19.
//

#include "switch.h"
TheSwitch::TheSwitch( const QString& text, QWidget* parent ) : QPushButton( text, parent ) {}

void TheSwitch::swapIcons() {
    if (counter % 2 == 0){
        this->setIcon(*pause_icon);
    }
    else {
        this->setIcon(*play_icon);
    }
}

void TheSwitch::addClick() {
    counter++;
}