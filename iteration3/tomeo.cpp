/*
 *
 *    ________________________________ _
 *   /_  __/___  ____ ___  ___  ____  | |
 *    / / / __ \/ __ `__ \/ _ \/ __ \ | |
 *   / / / /_/ / / / / / /  __/ /_/ / |_|
 *  /_/  \____/_/ /_/ /_/\___/\____/   _
 *              video for no reason   |_|
 *
 * 2811 cw2 November 2019 by twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QStateMachine>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QImageReader>
#include <QToolBar>
#include <QIcon>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QScrollBar>
#include <QObject>
#include "the_player.h"
#include "the_button.h"
#include "the_videowidget.h"
#include "switch.h"

using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

        if (!f.contains(".png")) { // if it isn't an image
            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // but a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        cerr << "warning: skipping video because I couldn't process thumbnail " << thumb.toStdString() << endl;
            }
            else
                cerr << "warning: skipping video because I couldn't find thumbnail " << thumb.toStdString() << endl;
        }
    }

    return out;
}

// FUNCTIONS
int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    cout << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // CREATE APPLICATION AND WINDOW
    QMainWindow *window = new QMainWindow();
    //window->setWindowFlags(Qt::WindowMinimizeButtonHint);
    TheVWidget *videoWidget = new TheVWidget();
    QWidget *buttonWidget = new QWidget();

    // LIST OF VIDEOS AND BUTTONS
    vector<TheButtonInfo> videos;
    vector<TheButton*> buttons;
    vector<QToolBar*> thumbnails;

    // ICON IMAGES
    QIcon *play_icon = new QIcon("icons/play1.png");
    QIcon *pause_icon = new QIcon("icons/pause1.png");
    QIcon *delete_icon = new QIcon("icons/delete.png");
    QIcon *rw_icon = new QIcon("icons/rw.png");
    QIcon *ff_icon = new QIcon("icons/ff.png");
    QIcon *add_icon = new QIcon("icons/add.png");

    // TOOLBARS
    //QToolBar *thumbnails = new QToolBar();
    QToolBar *tools = new QToolBar();
    tools->setFixedHeight(160);

    // IMPORT VIDEOS
    if (argc == 1)
        videos = getInfoIn(  "/tmp/2811_videos/");
    else
        videos = getInfoIn( string(argv[1]) );

    if (videos.size() == 0) {
        cerr << "no videos found! download from https://vcg.leeds.ac.uk/wp-content/static/2811/the/videos.zip into /tmp/2811_videos, and update the code on line 77";
        exit(-1);
    }

    // VIDEO PLAYER
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    //connect to fullscreen signal which fires upon fullscreen exit - reset player
    QObject::connect(videoWidget, &TheVWidget::fullscreenChanged, [videoWidget, window]() { videoWidget->resetPlayer(window, videoWidget); } );
    
    // the buttons are arranged horizontally
    QHBoxLayout *layout = new QHBoxLayout();
    buttonWidget->setLayout(layout);

    // create the four video buttons
    for ( int i = 0; i < 7; i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        //QPushButton *del = new QPushButton(*stop_icon, "", window);
        QToolBar *thumbnail = new QToolBar();
        thumbnail->setAllowedAreas(Qt::TopToolBarArea);
        thumbnail->setFloatable(false);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        thumbnail->addWidget(button);
        buttons.push_back(button);
        thumbnails.push_back(thumbnail);
        button->init(&videos.at(i));
    }

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos);

    // TOOLBAR
    QWidget *toolbar = new QWidget();   // widget

    // buttons
    QPushButton *rw = new QPushButton(*rw_icon, "", window);    // play
    rw->setIconSize(QSize(100, 100));
    rw->connect(rw, SIGNAL(released()), player, SLOT (playPrevious()));
    QPushButton *ff = new QPushButton(*ff_icon, "", window);    // play
    ff->setIconSize(QSize(100, 100));
    ff->connect(ff, SIGNAL(released()), player, SLOT (playNext()));
    QPushButton *add = new QPushButton(*add_icon, "", window);    // play
    add->setIconSize(QSize(40, 40));

    // test for play / pause
    //QPushButton *play = new QPushButton("", window);
    TheSwitch *play = new TheSwitch("", window);
    play->setIcon(*pause_icon);
    play->setIconSize(QSize(120, 120));

    play->connect(play, SIGNAL(released()), player, SLOT (addClick()));
    play->connect(play, SIGNAL(released()), player, SLOT (playPause()));

    play->connect(play, SIGNAL(released()), play, SLOT (addClick()));
    play->connect(play, SIGNAL(released()), play, SLOT (swapIcons()));


    // dummy spaces for centring the toolbar
    QHBoxLayout *tool_layout = new QHBoxLayout();
    auto space1 = new QWidget(toolbar);
    space1->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    auto space2 = new QWidget(toolbar);
    space2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    tool_layout->addWidget(add, 0, 0);
    tool_layout->addWidget(space1);
    tool_layout->addWidget(rw, 0, 0);
    tool_layout->addWidget(play, 0, 0);
    tool_layout->addWidget(ff, 0, 0);
    tool_layout->addWidget(space2);

    toolbar->setLayout(tool_layout);
    tools->addWidget(toolbar);

    // create the main window and layout
    //QWidget window;
    window->setWindowTitle("tomeo");
    window->setMinimumSize(800, 680);

    // add the video and the buttons to the top level widget
    window->setCentralWidget(videoWidget);
    window->addToolBar(Qt::BottomToolBarArea, tools);
    for(int i = 0; i < 7; i++) {
        window->addToolBar(Qt::TopToolBarArea, thumbnails[i]);
    }

    // showtime!
    window->show();

    // wait for the app to terminate
    return app.exec();
}

QWidget* setupToolBar () {


}