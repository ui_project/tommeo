/*
 *
 *    ________________________________ _
 *   /_  __/___  ____ ___  ___  ____  | |
 *    / / / __ \/ __ `__ \/ _ \/ __ \ | |
 *   / / / /_/ / / / / / /  __/ /_/ / |_|
 *  /_/  \____/_/ /_/ /_/\___/\____/   _
 *              video for no reason   |_|
 *
 * 2811 cw2 November 2019 by twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QImageReader>
#include <QToolBar>
#include <QIcon>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QScrollBar>
#include "the_player.h"
#include "the_button.h"

using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

        if (!f.contains(".png")) { // if it isn't an image
            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // but a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        cerr << "warning: skipping video because I couldn't process thumbnail " << thumb.toStdString() << endl;
            }
            else
                cerr << "warning: skipping video because I couldn't find thumbnail " << thumb.toStdString() << endl;
        }
    }

    return out;
}

// FUNCTIONS
QWidget* setupToolBar ();


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    cout << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // CREATE APPLICATION AND WINDOW
    QMainWindow *window = new QMainWindow();
    QVideoWidget *videoWidget = new QVideoWidget;
    QWidget *buttonWidget = new QWidget();

    // LIST OF VIDEOS AND BUTTONS
    vector<TheButtonInfo> videos;
    vector<TheButton*> buttons;

    // ICON IMAGES
    QIcon *play_icon = new QIcon("icons/play1.png");
    QIcon *pause_icon = new QIcon("icons/pause1.png");
    QIcon *stop_icon = new QIcon("icons/stop1.png");
    QIcon *rw_icon = new QIcon("icons/rw.png");
    QIcon *ff_icon = new QIcon("icons/ff.png");

    // TOOLBARS
    QToolBar *thumbnails = new QToolBar();
    QToolBar *tools = new QToolBar();
    thumbnails->setFixedHeight(160);
    tools->setFixedHeight(160);

    // IMPORT VIDEOS
    if (argc == 1)
        videos = getInfoIn(  "/tmp/2811_videos/");
    else
        videos = getInfoIn( string(argv[1]) );

    if (videos.size() == 0) {
        cerr << "no videos found! download from https://vcg.leeds.ac.uk/wp-content/static/2811/the/videos.zip into /tmp/2811_videos, and update the code on line 77";
        exit(-1);
    }

    // VIDEO PLAYER
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    // TOOLBAR
    QWidget *toolbar = new QWidget();   // widget

    // buttons
    QPushButton *play = new QPushButton(*play_icon, "", window);    // play
    play->setIconSize(QSize(100, 100));
    play->connect(play, SIGNAL(released()), player, SLOT (play()));

    QPushButton *pause = new QPushButton(*pause_icon, "", window);  // pause
    pause->setIconSize(QSize(80, 80));
    pause->connect(pause, SIGNAL(released()), player, SLOT (pause()));

    QPushButton *stop = new QPushButton(*stop_icon, "", window);    // stop
    stop->setIconSize(QSize(80, 80));
    stop->connect(stop, SIGNAL(released()), player, SLOT (stop()));
    
    QPushButton *rw = new QPushButton(*rw_icon, "", window);        // rw
    rw->setIconSize(QSize(80, 80));
    rw->connect(rw, SIGNAL(released()), player, SLOT (playPrevious()));

    QPushButton *ff = new QPushButton(*ff_icon, "", window);        // ff
    ff->setIconSize(QSize(80, 80));
    ff->connect(ff, SIGNAL(released()), player, SLOT (playNext()));

    // dummy spaces for centring the toolbar
    QHBoxLayout *tool_layout = new QHBoxLayout();
    auto space1 = new QWidget(toolbar);
    space1->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    auto space2 = new QWidget(toolbar);
    space2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    tool_layout->addWidget(space1);
    tool_layout->addWidget(stop,0,0);
    tool_layout->addWidget(pause, 0, 0);
    tool_layout->addWidget(play, 0, 0);
    tool_layout->addWidget(rw,0,0);
    tool_layout->addWidget(ff,0,0);
    tool_layout->addWidget(space2);

    toolbar->setLayout(tool_layout);
    tools->addWidget(toolbar);

    // the buttons are arranged horizontally
    QHBoxLayout *layout = new QHBoxLayout();
    buttonWidget->setLayout(layout);

    // create the four video buttons
    for ( int i = 0; i < 6; i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons.push_back(button);
        layout->addWidget(button, 0,0);
        button->init(&videos.at(i));
    }

    // SCROLL BAR FOR THUMBNAILS
    QScrollArea *image_wheel = new QScrollArea();
    image_wheel->setWidget(buttonWidget);
    image_wheel->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QScrollBar *scroll_bar = new QScrollBar();
    image_wheel->setHorizontalScrollBar(scroll_bar);
    thumbnails->addWidget(image_wheel);


    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos);

    // create the main window and layout
    //QWidget window;
    window->setWindowTitle("tomeo");
    window->setMinimumSize(800, 680);

    // add the video and the buttons to the top level widget
    window->setCentralWidget(videoWidget);
    window->addToolBar(Qt::TopToolBarArea, thumbnails);
    window->addToolBar(Qt::BottomToolBarArea, tools);

    // showtime!
    window->show();

    // wait for the app to terminate
    return app.exec();
}

QWidget* setupToolBar () {


}
